## Intro

This interpreter's syntax draws from the following sources:

* https://www.dartmouth.edu/basicfifty/basicmanual_1964.pdf - basic syntax
* https://www.dartmouth.edu/basicfifty/commands.html - additional commands

Some changes may have been made for usability purposes.

## Structure

The structure of a BASIC line always starts with a number. This is the line number that can be used to reference this line in the rest of the program.

    10 CODE HERE
    20 ANOTHER LINE OF CODE

It's common to increment lines by 10. This was because editing was difficult on the first BASIC-enabled computers, and incrementing by 10 allowed adding new lines later on without having to change other lines.

## Basic Data

### Strings

Strings are encapsulated in double quotes:

    "THIS IS A STRING 12345"
## Commands

### LET

The LET command defines a new variable. In the origina BASIC, the variable name can be a single letter with an optional number, for a total of 286 possible variables. In this version, variable names of any length are allowed.

### PRINT

The PRINT command outputs whatever it is given to the console:

    10 PRINT "HELLO WORLD"
    20 END
    ----
    HELLO WORLD

PRINT can print multiple items if they are separated by a comma

    10 PRINT "HELLO ", "WORLD"
    20 END
    ----
    HELLO WORLD

### END

The END command immediately ends the program. It is expected that every program will call END upon termination

### MATH

The following math operations are available:

* +
* *
* /
* -

These operations can be performed on numbers and variables:

    10 LET A = 10 + 5
    20 LET B = A + 5
    30 END

### FOR/NEXT

The FOR/NEXT commands can be used for a loop. The FOR command defines a loop counter and the two numbers to loop between. The NEXT indicates which loop counter to loop back on (this can allow for nested loops)

    10 FOR I = 1 TO 10
    20 LET A = I + 2
    30 NEXT I
    40 END

### GOTO

The GOTO statement immediately jumps program execution to another line

    10 GOTO 30
    20 PRINT "SKIPPED"
    30 PRINT "NOT SKIPPED"
    40 END
    ----
    NOT SKIPPED

### IF THEN

The IF THEN statement allows for branching control logic. It takes in a conditional and if the result is true, jumps to the given statement.

    10 LET A = 10
    20 IF A < 20 THEN 40
    30 PRINT "A IS BIG"
    40 PRINT "A IS ",A
    50 END
    ----
    A IS 10

### REM

Any line that starts with REM is a no-op and treated as a comment.

### GOSUB/RETURN

The GOSUB command allows the program to jump to another location, but remember where it jumped from, pushing that location to the stack. The RETURN command pops a location from the stack and goes to the line right after it.

    10 GOSUB 30
    20 END
    30 PRINT "HI"
    40 RETURN
    ----
    HI
    EXECUTION TERMINATED