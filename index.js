const fs = require("fs");

const args = process.argv.slice(2);
const file = args[0];

console.log(file);

const contents = fs.readFileSync(file, "utf8");

const charsOfInterest = ['"', " ", "\n", ";", ",", ">", "+", "-", "/", "*", "<", "="];
const tokens = [];
let buffer = "";
for (const char of contents) {
    console.log(char);
    if (charsOfInterest.includes(char)) {
        if (buffer.length > 0) {
            tokens.push(buffer);
        }
        tokens.push(char);
        buffer = "";
    } else {
        buffer += char;
    }
}

if (buffer.length > 0) {
    tokens.push(buffer);
}

console.log(tokens);

const statements = [];
const contextStack = [];

function pushContext() {
    contextStack.push(context);
    context = {};
}

function popContext(token) {
    if (contextStack.length === 0) {
        return;
    }
    const current = context;
    context = contextStack.pop();
    if (!context.children) {
        context.children = [];
    }
    context.children.push(current);

    state = "start";
    if (context.type === "let") {
        state = "letWithValue";
    } else if (context.type == "print") {
        if (token === ",") {
            state = "printBegin";
        } else {
            state = "printWithValue";
        }
    } else if (context.type === "for") {
        if (token === "to") {
            state = "forWithFirst";
        }
    } else if (context.type === "equation") {
        if (token === "then") {
            // we need to pop again
            popContext(token);
        }
    } else if (context.type === "if") {
        state = "ifWithCondition";
    }
}

function pushStatement() {
    statements.push(context);
    context = {};
}

// only called at the end of a line or at end of parsing
function popAllContext() {
    while (contextStack.length > 0) {
        popContext();
    }
    pushStatement();
    // we're at the beginning of the stack, so nothing left to do
    state = "start";
}

function startEquation(operator) {
    context = {
        leftHand: context,
        type: 'equation',
        operator,
    };
    state = "equationHasOperator";
}

const math = ["+", "-", "/", "*", "<", ">"]

let state = "start";
let context = {};
for (const upperToken of tokens) {
    const token = upperToken.toLowerCase();
    console.log(token, state, context);

    if (token === "\n") {
        popAllContext();
        continue;
    }

    if (state === "start") {
        if (`${parseInt(token)}` === token) {
            context.lineNumber = parseInt(token);
            state = "haveLineNumber";
            continue;
        }
    } else if (state === "statementStart") {
        if (token === " ") {
            continue;
        } else if (token === "\"") {
            context.string = "";
            context.type = "string";
            state = "stringBegin";
            continue;
        } else if (token === `${parseInt(token)}`) {
            context.number = parseInt(token);
            context.type = "number";
            state = "numberBeforeDot";
            continue;
        } else {
            context.variable = upperToken;
            context.type = "variable";
            state = "variable";
            continue;
        }
    } else if (state === "haveLineNumber") {
        if (token === " ") {
            continue;
        } else if (token === "print") {
            state = "printBegin";
            context.type = "print";
            continue;
        } else if (token === "let") {
            state = "letBegin";
            context.type = "let";
            continue;
        } else if (token === "end") {
            state = "end";
            context.type = "end";
            continue;
        } else if (token === "for") {
            state = "forBegin";
            context.type = "for";
            continue;
        } else if (token === "next") {
            state = "next";
            context.type = "next";
            continue;
        } else if (token === "goto") {
            state = "gotoStart";
            context.type = "goto";
            continue;
        } else if (token === "if") {
            state = "ifStart";
            context.type = "if";
            continue;
        } else if (token === "rem") {
            state = "rem";
            context.type = "rem";
            continue;
        } else if (token === "gosub") {
            state = "gosubStart";
            context.type = "gosub";
            continue;
        } else if (token === "return") {
            state = "return";
            context.type = "return";
            continue;
        }
    } else if (state === "printBegin") {
        if (token === " ") {
            continue;
        } else if (token === "\"") {
            pushContext();
            context.string = "";
            context.type = "string";
            state = "stringBegin";
            continue;
        } else {
            // assume token is a variable
            pushContext();
            context.variable = upperToken;
            context.type = "variable";
            state = "variable";
            continue;
        }
    } else if (state === "stringBegin") {
        if (token === "\"") {
            popContext(token);
            continue;
        }
        context.string += upperToken;
        continue;
    } else if (state === "letBegin") {
        if (token === " ") {
            continue;
        } else {
            context.variable = upperToken;
            state = "letWithVariable";
            continue;
        }
    } else if (state === "letWithVariable") {
        if (token === " ") {
            continue;
        } else if (token === "=") {
            pushContext();
            state = "statementStart";
            continue;
        }
    } else if (state === "printWithValue") {
        if (token === ",") {
            state = "printBegin";
            continue;
        } else if (token === " ") {
            continue;
        } else {
            pushContext();
            context.type = "variable";
            context.variable = upperToken;
            state = "variable";
            continue;
        }
    } else if (state === "numberBeforeDot") {
        if (token === " ") {
            continue;
        } else if (math.includes(token)) {
            startEquation(token);
            continue;
        } else if (token === "then") {
            popContext(token);
            continue;
        }
    } else if (state === "equationHasOperator") {
        if (token === " ") {
            continue;
        } else if (token === `${parseInt(token)}`) {
            pushContext();
            state = "numberBeforeDot";
            context.type = "number";
            context.number = parseInt(token);
            continue;
        } else {
            pushContext();
            state = "variable";
            context.type = "variable";
            context.variable = token;
            continue;
        }
    } else if (state === "variable") {
        if (token === " ") {
            continue;
        } else if (math.includes(token)) {
            startEquation(token);
            continue;
        } else if (token === ",") {
            popContext(token);
            continue;
        } else if (token === "then") {
            popContext(token);
            continue;
        }
    } else if (state === "forBegin") {
        if (token === " ") {
            continue;
        } else {
            context.variable = upperToken;
            state = "forWithVariable";
            continue;
        }
    } else if (state === "forWithVariable") {
        if (token === " ") {
            continue;
        } else if (token === "=") {
            state = "forWithEquals";
            continue;
        }
    } else if (state === "forWithEquals") {
        if (token === " ") {
            continue;
        } else if (token === `${parseInt(token)}`) {
            pushContext();
            context.type = "number";
            context.number = parseInt(token);
            state = "numberForLoop";
            continue;
        }
    } else if (state === "numberForLoop") {
        if (token === " ") {
            continue;
        } else if (token === "to") {
            popContext(token)
            continue;
        }
    } else if (state === "forWithFirst") {
        if (token === " ") {
            continue;
        } else if (token === `${parseInt(token)}`) {
            pushContext();
            context.type = "number";
            context.number = parseInt(token);
            state = "numberForLoop";
            continue;
        }
    } else if (state === "next") {
        if (token === " ") {
            continue;
        } else {
            context.variable = upperToken;
            state = "nextWithVariable";
            continue;
        }
    } else if (state === "gotoStart") {
        if (token === " ") {
            continue;
        } else if (token === `${parseInt(token)}`) {
            context.line = parseInt(token);
            state = "gotoWithLine";
            continue;
        }
    } else if (state === "ifStart") {
        if (token === " ") {
            continue;
        } else if (token === `${parseInt(token)}`) {
            pushContext();
            context.type = "number";
            context.number = token;
            state = "numberBeforeDot";
            continue;
        } else {
            pushContext();
            context.type = "variable";
            context.variable = token;
            state = "variable";
            continue;
        }
    } else if (state === "ifWithCondition") {
        if (token === " ") {
            continue;
        } else if (token === `${parseInt(token)}`) {
            context.line = parseInt(token);
            continue;
        }
    } else if (state === "rem") {
        // rem ignores everything until end of line
        continue;
    } else if (state === "gosubStart") {
        if (token === " ") {
            continue;
        } else if (token === `${parseInt(token)}`) {
            context.line = parseInt(token);
            continue;
        }
    }
    throw new Error("Unexpected \"" + upperToken + "\" with state " + state);
}

// unroll all context
popAllContext();

console.log(JSON.stringify(statements, null, 4));

// get program by lines
const codeByLine = {};
for (const statement of statements) {
    codeByLine[statement.lineNumber] = statement;
}

console.log(codeByLine);

const allLines = Object.keys(codeByLine).sort();
let currentLine = allLines[0];

const programState = {
    currentLine: allLines[0],
    variables: {},
    returnStack: [],
};

function terminate() {
    console.log("EXECUTION TERMINATED");
    process.exit(0);
}

function processAllCode(list) {
    return list.map((item) => {
        return processCode(item);
    });
}

function findAndGotoLine({ match, next, backwards }) {
    let linesCopy = [...allLines];
    if (backwards) {
        linesCopy = linesCopy.reverse();
    }

    let curIndex = linesCopy.indexOf(programState.currentLine);

    let matchingLine = null;
    for (let i=curIndex;i<linesCopy.length;i++) {
        const programAtLine = codeByLine[linesCopy[i]];
        // check match
        if (!match) {
            matchingLine = linesCopy[i];
            break;
        }

        let match2 = true;
        for (const key in match) {
            //console.log(programAtLine[key], match[key]);
            if (programAtLine[key] !== match[key]) {
                match2 = false;
                break;
            }
        }
        if (match2) {
            matchingLine = linesCopy[i];
            break;
        }
    }

    //console.log("matching is", matchingLine);

    if (matchingLine !== null) {
        if (next) {
            // now we need to find the next line in the actual program code
            const index = allLines.indexOf(matchingLine);
            matchingLine = allLines[index+1];
        }

        programState.currentLine = matchingLine;
    }
}

function processCode(line) {
    let data1;
    let data2;
    switch (line.type) {
        case "print":
            data1 = processAllCode(line.children);
            console.log(data1.join(""));
            break;
        case "string":
            return line.string;
        case "let":
            data1 = processCode(line.children[0]);
            programState.variables[line.variable] = data1;
            break;
        case "variable":
            return programState.variables[line.variable];
        case "end":
            terminate();
            break;
        case "equation":
            //console.log(line);
            data1 = processCode(line.leftHand);
            data2 = processAllCode(line.children);
            // this is bit complex so bears explaining. Basically
            // the acc starts with the first item of data2, then
            // we slice to ensure we don't reduce over the first item (since
            // that would cause it to add twice)
            data2 = data2.slice(1).reduce((acc, val) => {
                if (line.operator === "+") {
                    return acc + val;
                } else if (line.operator === "*") {
                    return acc * val;
                } else if (line.operator === "/") {
                    return acc / val;
                } else if (line.operator === "-") {
                    return data1 - data2;
                } else {
                    throw new Error("Unrecognized operation " + line.operator);
                }
            },data2[0]);

            //console.log(data1, data2);

            if (line.operator === "+") {
                return data1 + data2;
            } else if (line.operator === "*") {
                return data1 * data2;
            } else if (line.operator === "/") {
                return data1 / data2;
            } else if (line.operator === "-") {
                return data1 - data2;
            } else if (line.operator === ">") {
                return data1 > data2;
            } else if (line.operator === "<") {
                return data1 < data2;
            } else {
                throw new Error("Unrecognized operation " + line.operator);
            }
        case "number":
            return line.number;
        case "for":
            //console.log("for");
            if (!programState.variables[line.variable]) {
                programState.variables[line.variable] = processCode(line.children[0]);
                //console.log("starting with",programState.variables[line.variable]);
            } else {
                data1 = processCode(line.children[1]);
                if (programState.variables[line.variable] >= data1) {
                    //console.log("at end!",programState.variables[line.variable], data1);
                    // find the matching NEXT and go there, but go to the statement after
                    findAndGotoLine({
                        match: {
                            type: 'next',
                            variable: line.variable,
                        },
                        next: true,
                    });
                    return true;
                } else {
                    programState.variables[line.variable] ++;
                    //console.log("incrementing", programState.variables[line.variable]);
                }
            }

            return false;
        case "next":
            // search backwards in code for the matching FOR and go there.
            findAndGotoLine({
                match: {
                    type: 'for',
                    variable: line.variable,
                },
                next: false,
                backwards: true,
            });

            return true;
        case "goto":
            // this has to be this way so the lookup works later on
            programState.currentLine = `${line.line}`;
            return true;
        case "if":
            data1 = processCode(line.children[0]);
            if (data1 === true) {
                programState.currentLine = `${line.line}`;
                return true;
            }
            break;
        case "rem":
            // noop
            break;
        case "gosub":
            programState.returnStack.push(programState.currentLine);
            programState.currentLine = `${line.line}`;
            return true;
        case "return":
            // in this case we do not return true because we actually want the
            // system to advance to the next line once we are done
            programState.currentLine = `${programState.returnStack.pop()}`;
            break;
        default:
            console.log(JSON.stringify(line, null, 4));
            throw new Error(`Unknown type ${line.type}`);
    }
}

while (true) {
    const line = codeByLine[programState.currentLine];

    //console.log("Executing", programState.currentLine, line);

    const changedExecution = processCode(line);

    //console.log("changed?", changedExecution, programState.currentLine);

    if (!changedExecution) {
        // find the next line to run, but only if some other code we just ran
        // didn't change our execution path
        const index = allLines.indexOf(programState.currentLine);
        //console.log("index of previous line is", index, allLines);
        // we do not check if we're off the end here because the last line should be an END
        programState.currentLine = allLines[index + 1];
        //console.log("next line is", programState.currentLine);
    }
}